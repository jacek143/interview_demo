/*******************************************************************************
*
* E M B E D D E D   W I Z A R D   P R O J E C T
*
*                                                Copyright (c) TARA Systems GmbH
*                                    written by Paul Banach and Manfred Schweyer
*
********************************************************************************
*
* This software is delivered "as is" and shows the usage of other software
* components. It is provided as an example software which is intended to be
* modified and extended according to particular requirements.
*
* TARA Systems hereby disclaims all warranties and conditions with regard to the
* software, including all implied warranties and conditions of merchantability
* and non-infringement of any third party IPR or other rights which may result
* from the use or the inability to use the software.
*
********************************************************************************
*
* DESCRIPTION:
*
*   Template
*
*   This directory contains the necessary source codes and makefiles to compile
*   and link an Embedded Wizard generated application for a dedicated STM32
*   target. It is just a template for convenience.
*
*   This package is prepared and tested for the STM32F746 Discovery board.
*
*******************************************************************************/

Getting started with STM32F746 Discovery board:
-----------------------------------------------
  In order to get your first Embedded Wizard generated UI application up and
  running on your STM32 target, we have prepared a detailed article, which
  covers all necessary steps.
  We highly recommend to study the following document:

  http://doc.embedded-wizard.de/getting-started-stm32f746-discovery

How to build a demo using this tempalte for the STM32F746 Discovery board:
--------------------------------------------------------------------------

  1.) Start Embedded Wizard Studio and open any project.
      Create a profile for the STM32 target and make all settings so that
      it fits to your target (e.g. PlatformPackage, ScreenSize, ScreenOrientation).

      Set the output directory to the subdirectory /Template/GeneratedCode.

      => Generate code by pressing 'F8'

   2.) Execute the batch file 'StartBuildEnvironment.bat' you will find in
       the directory \Build. As a result a console window will appear.

   3.) Compile and link the example:

       make

   4.) If the application was compiled and linked successfully, connect your
       target and download the application:

       make install

   5.) If necessary, adapt the makefile to your needs.
