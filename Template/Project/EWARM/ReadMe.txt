/*******************************************************************************
*
* E M B E D D E D   W I Z A R D   P R O J E C T
*
*                                                Copyright (c) TARA Systems GmbH
*                                    written by Paul Banach and Manfred Schweyer
*
********************************************************************************
*
* This software is delivered "as is" and shows the usage of other software
* components. It is provided as an example software which is intended to be
* modified and extended according to particular requirements.
*
* TARA Systems hereby disclaims all warranties and conditions with regard to the
* software, including all implied warranties and conditions of merchantability
* and non-infringement of any third party IPR or other rights which may result
* from the use or the inability to use the software.
*
********************************************************************************


Embedded Wizard STM STM32 IAR Embedded Workbench - ReadMe
--------------------------------------------------------------------------------


This file describes how to build the Embedded Wizard examples using
IAR Embedded Workbench.

- The Embedded Wizard Template project is commonly used for all provided 
  Embedded Wizard examples. All Embedded Wizard examples will store the 
  generated code within the common \Template\GeneratedCode folder..

- The generated code of an Embedded Wizard example is imported automatically
  to the IAR Embedded Workbench project using the Project Connection mechanism.

- To establish this automatic project import a post process has to be added
  to the Profile settings within Embedded Wizard Studio:
  within Embedded Wizard Studio:
  * Open the desired Embedded Wizard example project.
  * Select the profile and set EWARM_ew_post_process.cmd as post process 
    at the Inspector view. You will find the file within the 
    subdirectory \Template\Project\EWARM.

- After the Embedded Wizard code generation the installed post process 
  will generate a ewfiles.ipcf file, that controls the import to the
  IAR Embedded Workbench project.

- After returning to IAR Embedded Workbench, the latest generated code and
  the suitable Embedded Wizard Platform Package will be imported to the
  IAR Embedded Workbench project (depending on the color format and the
  screen orientation selected in the Embedded Wizard Profile).

- If the color format or the screen orientation was changed, please do a 
  complete rebuild of the IAR Embedded Workbench project.
  
- Do not use an IAR compiler optimisation level above 'medium', since 
  drawing errors can occur using higer compiler optimization levels.
  
--------------------------------------------------------------------------------
