#ifndef __tim_H
#define __tim_H
#ifdef __cplusplus
 extern "C" {
#endif

#include "stm32f7xx_hal.h"

extern TIM_HandleTypeDef htim3;
extern void _Error_Handler(char *, int);

void TIM3_Init(void);
void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

#ifdef __cplusplus
}
#endif
#endif /*__ tim_H */


