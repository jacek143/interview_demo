/*******************************************************************************
*
* E M B E D D E D   W I Z A R D   P R O J E C T
*
*                                                Copyright (c) TARA Systems GmbH
*                                    written by Paul Banach and Manfred Schweyer
*
********************************************************************************
*
* This file was generated automatically by Embedded Wizard. Please do not make 
* any modifications of this file! The modifications are lost when the file is
* generated again by Embedded Wizard!
*
* The template of this heading text can be found in the file 'head.ewt' in the
* directory 'Platforms' of your Embedded Wizard installation directory. If you
* wish to adapt this text, please copy the template file 'head.ewt' into your
* project directory and edit the copy only. Please avoid any modifications of
* the original template file!
*
* Version  : 8.20
* Profile  : STM32F746
* Platform : STM.STM32.RGB565
*
*******************************************************************************/

#include "ewlocale.h"
#include "_ApplicationApplication.h"
#include "_ApplicationDeviceClass.h"
#include "_CoreView.h"
#include "_FlatHorzSlider.h"
#include "_FlatIndicator.h"
#include "_FlatLabel.h"
#include "_FlatSwitch.h"
#include "_ViewsLine.h"
#include "_ViewsRectangle.h"
#include "Application.h"

/* Compressed strings for the language 'Default'. */
static const unsigned int _StringsDefault0[] =
{
  0x00000024, /* ratio 100.00 % */
  0xB8000F00, 0x00000452, 0x120A0026, 0xC1200831, 0xA60082C0, 0x05200424, 0x4F001580,
  0x080C6A18, 0x00000000
};

/* Constant values used in this 'C' module only. */
static const XRect _Const0000 = {{ 0, 0 }, { 480, 272 }};
static const XColor _Const0001 = { 0x00, 0x00, 0x00, 0xFF };
static const XRect _Const0002 = {{ 100, 0 }, { 160, 45 }};
static const XRect _Const0003 = {{ 60, 10 }, { 90, 40 }};
static const XRect _Const0004 = {{ 10, 10 }, { 50, 40 }};
static const XStringRes _Const0005 = { _StringsDefault0, 0x0003 };
static const XPoint _Const0006 = { 170, 50 };
static const XPoint _Const0007 = { 0, 50 };
static const XPoint _Const0008 = { 170, 0 };
static const XRect _Const0009 = {{ 110, 210 }, { 460, 270 }};
static const XRect _Const000A = {{ 10, 220 }, { 90, 250 }};
static const XStringRes _Const000B = { _StringsDefault0, 0x000A };
static const XPoint _Const000C = { 480, 200 };
static const XPoint _Const000D = { 0, 200 };

/* Initializer for the class 'Application::Application' */
void ApplicationApplication__Init( ApplicationApplication _this, XObject aLink, XHandle aArg )
{
  /* At first initialize the super class ... */
  CoreRoot__Init( &_this->_Super, aLink, aArg );

  /* ... then construct all embedded objects */
  ViewsRectangle__Init( &_this->Rectangle, &_this->_XObject, 0 );
  FlatSwitch__Init( &_this->Switch1, &_this->_XObject, 0 );
  FlatIndicator__Init( &_this->Indicator, &_this->_XObject, 0 );
  FlatLabel__Init( &_this->Label, &_this->_XObject, 0 );
  ViewsLine__Init( &_this->Line, &_this->_XObject, 0 );
  ViewsLine__Init( &_this->Line1, &_this->_XObject, 0 );
  FlatHorzSlider__Init( &_this->HorzSlider3, &_this->_XObject, 0 );
  FlatLabel__Init( &_this->Label1, &_this->_XObject, 0 );
  ViewsLine__Init( &_this->Line2, &_this->_XObject, 0 );

  /* Setup the VMT pointer */
  _this->_VMT = EW_CLASS( ApplicationApplication );

  /* ... and initialize objects, variables, properties, etc. */
  CoreRectView__OnSetBounds( &_this->Rectangle, _Const0000 );
  ViewsRectangle_OnSetColor( &_this->Rectangle, _Const0001 );
  CoreRectView__OnSetBounds( _this, _Const0000 );
  CoreRectView__OnSetBounds( &_this->Switch1, _Const0002 );
  CoreRectView__OnSetBounds( &_this->Indicator, _Const0003 );
  CoreRectView__OnSetBounds( &_this->Label, _Const0004 );
  FlatLabel_OnSetString( &_this->Label, EwLoadString( &_Const0005 ));
  CoreLineView_OnSetPoint2((CoreLineView)&_this->Line, _Const0006 );
  CoreLineView_OnSetPoint1((CoreLineView)&_this->Line, _Const0007 );
  CoreLineView_OnSetPoint2((CoreLineView)&_this->Line1, _Const0006 );
  CoreLineView_OnSetPoint1((CoreLineView)&_this->Line1, _Const0008 );
  CoreRectView__OnSetBounds( &_this->HorzSlider3, _Const0009 );
  FlatHorzSlider_OnSetMaxValue( &_this->HorzSlider3, 100 );
  FlatHorzSlider_OnSetCurrentValue( &_this->HorzSlider3, 0 );
  CoreRectView__OnSetBounds( &_this->Label1, _Const000A );
  FlatLabel_OnSetString( &_this->Label1, EwLoadString( &_Const000B ));
  CoreLineView_OnSetPoint2((CoreLineView)&_this->Line2, _Const000C );
  CoreLineView_OnSetPoint1((CoreLineView)&_this->Line2, _Const000D );
  CoreGroup__Add( _this, ((CoreView)&_this->Rectangle ), 0 );
  CoreGroup__Add( _this, ((CoreView)&_this->Switch1 ), 0 );
  CoreGroup__Add( _this, ((CoreView)&_this->Indicator ), 0 );
  CoreGroup__Add( _this, ((CoreView)&_this->Label ), 0 );
  CoreGroup__Add( _this, ((CoreView)&_this->Line ), 0 );
  CoreGroup__Add( _this, ((CoreView)&_this->Line1 ), 0 );
  CoreGroup__Add( _this, ((CoreView)&_this->HorzSlider3 ), 0 );
  CoreGroup__Add( _this, ((CoreView)&_this->Label1 ), 0 );
  CoreGroup__Add( _this, ((CoreView)&_this->Line2 ), 0 );
  FlatSwitch_OnSetOutlet( &_this->Switch1, EwNewRef( EwGetAutoObject( &ApplicationDevice, 
  ApplicationDeviceClass ), ApplicationDeviceClass_OnGetLedState, ApplicationDeviceClass_OnSetLedState 
  ));
  FlatIndicator_OnSetIcon( &_this->Indicator, 0 );
  FlatIndicator_OnSetOutlet( &_this->Indicator, EwNewRef( EwGetAutoObject( &ApplicationDevice, 
  ApplicationDeviceClass ), ApplicationDeviceClass_OnGetLedState, ApplicationDeviceClass_OnSetLedState 
  ));
  FlatHorzSlider_OnSetOutlet( &_this->HorzSlider3, EwNewRef( EwGetAutoObject( &ApplicationDevice, 
  ApplicationDeviceClass ), ApplicationDeviceClass_OnGetServoState, ApplicationDeviceClass_OnSetServoState 
  ));
}

/* Re-Initializer for the class 'Application::Application' */
void ApplicationApplication__ReInit( ApplicationApplication _this )
{
  /* At first re-initialize the super class ... */
  CoreRoot__ReInit( &_this->_Super );

  /* ... then re-construct all embedded objects */
  ViewsRectangle__ReInit( &_this->Rectangle );
  FlatSwitch__ReInit( &_this->Switch1 );
  FlatIndicator__ReInit( &_this->Indicator );
  FlatLabel__ReInit( &_this->Label );
  ViewsLine__ReInit( &_this->Line );
  ViewsLine__ReInit( &_this->Line1 );
  FlatHorzSlider__ReInit( &_this->HorzSlider3 );
  FlatLabel__ReInit( &_this->Label1 );
  ViewsLine__ReInit( &_this->Line2 );
}

/* Finalizer method for the class 'Application::Application' */
void ApplicationApplication__Done( ApplicationApplication _this )
{
  /* Finalize this class */
  _this->_VMT = EW_CLASS( ApplicationApplication );

  /* Finalize all embedded objects */
  ViewsRectangle__Done( &_this->Rectangle );
  FlatSwitch__Done( &_this->Switch1 );
  FlatIndicator__Done( &_this->Indicator );
  FlatLabel__Done( &_this->Label );
  ViewsLine__Done( &_this->Line );
  ViewsLine__Done( &_this->Line1 );
  FlatHorzSlider__Done( &_this->HorzSlider3 );
  FlatLabel__Done( &_this->Label1 );
  ViewsLine__Done( &_this->Line2 );

  /* Don't forget to deinitialize the super class ... */
  CoreRoot__Done( &_this->_Super );
}

/* Garbage Collector method for the class 'Application::Application' */
void ApplicationApplication__Mark( ApplicationApplication _this )
{
  EwMarkObject( &_this->Rectangle );
  EwMarkObject( &_this->Switch1 );
  EwMarkObject( &_this->Indicator );
  EwMarkObject( &_this->Label );
  EwMarkObject( &_this->Line );
  EwMarkObject( &_this->Line1 );
  EwMarkObject( &_this->HorzSlider3 );
  EwMarkObject( &_this->Label1 );
  EwMarkObject( &_this->Line2 );

  /* Give the super class a chance to mark its objects and references */
  CoreRoot__Mark( &_this->_Super );
}

/* Variants derived from the class : 'Application::Application' */
EW_DEFINE_CLASS_VARIANTS( ApplicationApplication )
EW_END_OF_CLASS_VARIANTS( ApplicationApplication )

/* Virtual Method Table (VMT) for the class : 'Application::Application' */
EW_DEFINE_CLASS( ApplicationApplication, CoreRoot, "Application::Application" )
  CoreRectView_initLayoutContext,
  CoreRoot_GetRoot,
  CoreRoot_Draw,
  CoreView_HandleEvent,
  CoreGroup_CursorHitTest,
  CoreRectView_ArrangeView,
  CoreRectView_MoveView,
  CoreRectView_GetExtent,
  CoreGroup_ChangeViewState,
  CoreGroup_OnSetBounds,
  CoreRoot_OnSetFocus,
  CoreRoot_DispatchEvent,
  CoreRoot_BroadcastEvent,
  CoreGroup_UpdateLayout,
  CoreGroup_UpdateViewState,
  CoreRoot_InvalidateArea,
  CoreRoot_Restack,
  CoreRoot_Add,
EW_END_OF_CLASS( ApplicationApplication )

/* User defined inline code: 'Application::Inline' */
#include "DeviceDriver.h"

/* Initializer for the class 'Application::DeviceClass' */
void ApplicationDeviceClass__Init( ApplicationDeviceClass _this, XObject aLink, XHandle aArg )
{
  /* At first initialize the super class ... */
  TemplatesDeviceClass__Init( &_this->_Super, aLink, aArg );

  /* Setup the VMT pointer */
  _this->_VMT = EW_CLASS( ApplicationDeviceClass );
}

/* Re-Initializer for the class 'Application::DeviceClass' */
void ApplicationDeviceClass__ReInit( ApplicationDeviceClass _this )
{
  /* At first re-initialize the super class ... */
  TemplatesDeviceClass__ReInit( &_this->_Super );
}

/* Finalizer method for the class 'Application::DeviceClass' */
void ApplicationDeviceClass__Done( ApplicationDeviceClass _this )
{
  /* Finalize this class */
  _this->_VMT = EW_CLASS( ApplicationDeviceClass );

  /* Don't forget to deinitialize the super class ... */
  TemplatesDeviceClass__Done( &_this->_Super );
}

/* Garbage Collector method for the class 'Application::DeviceClass' */
void ApplicationDeviceClass__Mark( ApplicationDeviceClass _this )
{
  /* Give the super class a chance to mark its objects and references */
  TemplatesDeviceClass__Mark( &_this->_Super );
}

/* 'C' function for method : 'Application::DeviceClass.OnSetLedState()' */
void ApplicationDeviceClass_OnSetLedState( ApplicationDeviceClass _this, XBool value )
{
  if ( _this->LedState == value )
    return;

  _this->LedState = value;
  DeviceDriver_SetLedStatus( value );
  EwNotifyRefObservers( EwNewRef( _this, ApplicationDeviceClass_OnGetLedState, ApplicationDeviceClass_OnSetLedState 
    ), 0 );
}

/* 'C' function for method : 'Application::DeviceClass.OnSetServoState()' */
void ApplicationDeviceClass_OnSetServoState( ApplicationDeviceClass _this, XInt32 
  value )
{
  if ( _this->ServoState == value )
    return;

  _this->ServoState = value;
  DeviceDriver_SetServo(value);
  EwNotifyRefObservers( EwNewRef( _this, ApplicationDeviceClass_OnGetServoState, 
    ApplicationDeviceClass_OnSetServoState ), 0 );
}

/* Default onget method for the property 'LedState' */
XBool ApplicationDeviceClass_OnGetLedState( ApplicationDeviceClass _this )
{
  return _this->LedState;
}

/* Default onget method for the property 'ServoState' */
XInt32 ApplicationDeviceClass_OnGetServoState( ApplicationDeviceClass _this )
{
  return _this->ServoState;
}

/* Variants derived from the class : 'Application::DeviceClass' */
EW_DEFINE_CLASS_VARIANTS( ApplicationDeviceClass )
EW_END_OF_CLASS_VARIANTS( ApplicationDeviceClass )

/* Virtual Method Table (VMT) for the class : 'Application::DeviceClass' */
EW_DEFINE_CLASS( ApplicationDeviceClass, TemplatesDeviceClass, "Application::DeviceClass" )
EW_END_OF_CLASS( ApplicationDeviceClass )

/* User defined auto object: 'Application::Device' */
EW_DEFINE_AUTOOBJECT( ApplicationDevice, ApplicationDeviceClass )

/* Initializer for the auto object 'Application::Device' */
void ApplicationDevice__Init( ApplicationDeviceClass _this )
{
  EW_UNUSED_ARG( _this );
}

/* Table with links to derived variants of the auto object : 'Application::Device' */
EW_DEFINE_AUTOOBJECT_VARIANTS( ApplicationDevice )
EW_END_OF_AUTOOBJECT_VARIANTS( ApplicationDevice )

/* Embedded Wizard */
