/*******************************************************************************
*
* E M B E D D E D   W I Z A R D   P R O J E C T
*
*                                                Copyright (c) TARA Systems GmbH
*                                    written by Paul Banach and Manfred Schweyer
*
********************************************************************************
*
* This file was generated automatically by Embedded Wizard. Please do not make 
* any modifications of this file! The modifications are lost when the file is
* generated again by Embedded Wizard!
*
* The template of this heading text can be found in the file 'head.ewt' in the
* directory 'Platforms' of your Embedded Wizard installation directory. If you
* wish to adapt this text, please copy the template file 'head.ewt' into your
* project directory and edit the copy only. Please avoid any modifications of
* the original template file!
*
* Version  : 8.20
* Profile  : STM32F746
* Platform : STM.STM32.RGB565
*
*******************************************************************************/

#ifndef _SteelSwitch_H
#define _SteelSwitch_H

#ifdef __cplusplus
  extern "C"
  {
#endif

#include "ewrte.h"
#if EW_RTE_VERSION != 0x00080014
  #error Wrong version of Embedded Wizard Runtime Environment.
#endif

#include "ewgfx.h"
#if EW_GFX_VERSION != 0x00080014
  #error Wrong version of Embedded Wizard Graphics Engine.
#endif

#include "_CoreGroup.h"
#include "_CoreKeyPressHandler.h"
#include "_CoreSimpleTouchHandler.h"
#include "_ViewsImage.h"

/* Forward declaration of the class Core::LayoutContext */
#ifndef _CoreLayoutContext_
  EW_DECLARE_CLASS( CoreLayoutContext )
#define _CoreLayoutContext_
#endif

/* Forward declaration of the class Core::View */
#ifndef _CoreView_
  EW_DECLARE_CLASS( CoreView )
#define _CoreView_
#endif

/* Forward declaration of the class Graphics::Canvas */
#ifndef _GraphicsCanvas_
  EW_DECLARE_CLASS( GraphicsCanvas )
#define _GraphicsCanvas_
#endif

/* Forward declaration of the class Steel::Switch */
#ifndef _SteelSwitch_
  EW_DECLARE_CLASS( SteelSwitch )
#define _SteelSwitch_
#endif


/* Switch widget with a steel design. The widget is used to switch a boolean value 
   on and off. The appearance is similar to a switch with on/off position. */
EW_DEFINE_FIELDS( SteelSwitch, CoreGroup )
  EW_PROPERTY( OnSwitchOff,     XSlot )
  EW_PROPERTY( OnSwitchOn,      XSlot )
  EW_PROPERTY( OnChange,        XSlot )
  EW_OBJECT  ( Image,           ViewsImage )
  EW_OBJECT  ( Knob,            ViewsImage )
  EW_OBJECT  ( TouchHandler,    CoreSimpleTouchHandler )
  EW_PROPERTY( Outlet,          XRef )
  EW_OBJECT  ( KeyHandlerOk,    CoreKeyPressHandler )
  EW_PROPERTY( Active,          XBool )
  EW_RESERVED( 3 )
EW_END_OF_FIELDS( SteelSwitch )

/* Virtual Method Table (VMT) for the class : 'Steel::Switch' */
EW_DEFINE_METHODS( SteelSwitch, CoreGroup )
  EW_METHOD( initLayoutContext, void )( CoreRectView _this, XRect aBounds, CoreOutline 
    aOutline )
  EW_METHOD( GetRoot,           CoreRoot )( CoreView _this )
  EW_METHOD( Draw,              void )( CoreGroup _this, GraphicsCanvas aCanvas, 
    XRect aClip, XPoint aOffset, XInt32 aOpacity, XBool aBlend )
  EW_METHOD( HandleEvent,       XObject )( CoreView _this, CoreEvent aEvent )
  EW_METHOD( CursorHitTest,     CoreCursorHit )( CoreGroup _this, XRect aArea, XInt32 
    aFinger, XInt32 aStrikeCount, CoreView aDedicatedView, XSet aRetargetReason )
  EW_METHOD( ArrangeView,       XPoint )( CoreRectView _this, XRect aBounds, XEnum 
    aFormation )
  EW_METHOD( MoveView,          void )( CoreRectView _this, XPoint aOffset, XBool 
    aFastMove )
  EW_METHOD( GetExtent,         XRect )( CoreRectView _this )
  EW_METHOD( ChangeViewState,   void )( CoreGroup _this, XSet aSetState, XSet aClearState )
  EW_METHOD( OnSetBounds,       void )( CoreGroup _this, XRect value )
  EW_METHOD( OnSetFocus,        void )( CoreGroup _this, CoreView value )
  EW_METHOD( DispatchEvent,     XObject )( CoreGroup _this, CoreEvent aEvent )
  EW_METHOD( BroadcastEvent,    XObject )( CoreGroup _this, CoreEvent aEvent, XSet 
    aFilter )
  EW_METHOD( UpdateViewState,   void )( SteelSwitch _this, XSet aState )
  EW_METHOD( InvalidateArea,    void )( CoreGroup _this, XRect aArea )
  EW_METHOD( Restack,           void )( CoreGroup _this, CoreView aView, XInt32 
    aOrder )
  EW_METHOD( Add,               void )( CoreGroup _this, CoreView aView, XInt32 
    aOrder )
EW_END_OF_METHODS( SteelSwitch )

/* The method UpdateViewState() is invoked automatically after the state of the 
   component has been changed. This method can be overridden and filled with logic 
   to ensure the visual aspect of the component does reflect its current state. 
   For example, the 'enabled' state of the component can affect its colors (disabled 
   components may appear pale). In this case the logic of the method should modify 
   the respective color properties accordingly to the current 'enabled' state. 
   The current state of the component is passed as a set in the parameter aState. 
   It reflects the very basic component state like its visibility or the ability 
   to react to user inputs. Beside this common state, the method can also involve 
   any other variables used in the component as long as they reflect its current 
   state. For example, the toggle switch component can take in account its toggle 
   state 'on' or 'off' and change accordingly the location of the slider, etc.
   Usually, this method will be invoked automatically by the framework. Optionally 
   you can request its invocation by using the method @InvalidateViewState(). */
void SteelSwitch_UpdateViewState( SteelSwitch _this, XSet aState );

/* This internal slot method is used to receive the corresponding signals form the 
   touch handler. */
void SteelSwitch_pressReleaseSlot( SteelSwitch _this, XObject sender );

/* 'C' function for method : 'Steel::Switch.OnSetActive()' */
void SteelSwitch_OnSetActive( SteelSwitch _this, XBool value );

/* This slot method will receive a signal, if the value of the property assigned 
   to @Outlet has been changed by another widget or by the application logic. In 
   response to this notification, the widget will update itself. */
void SteelSwitch_outletSlot( SteelSwitch _this, XObject sender );

/* 'C' function for method : 'Steel::Switch.OnSetOutlet()' */
void SteelSwitch_OnSetOutlet( SteelSwitch _this, XRef value );

/* This internal slot method is used to receive the corresponding signals form the 
   touch handler. */
void SteelSwitch_enterLeaveSlot( SteelSwitch _this, XObject sender );

/* This internal slot method is used to receive the corresponding signal form the 
   keyboard handler. */
void SteelSwitch_onOkSlot( SteelSwitch _this, XObject sender );

#ifdef __cplusplus
  }
#endif

#endif /* _SteelSwitch_H */

/* Embedded Wizard */
