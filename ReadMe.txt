﻿/*******************************************************************************
*
* E M B E D D E D   W I Z A R D   P R O J E C T
*
*                                                Copyright (c) TARA Systems GmbH
*                                    written by Paul Banach and Manfred Schweyer
*
********************************************************************************
*
* This software is delivered "as is" and shows the usage of other software
* components. It is provided as an example software which is intended to be
* modified and extended according to particular requirements.
*
* TARA Systems hereby disclaims all warranties and conditions with regard to the
* software, including all implied warranties and conditions of merchantability
* and non-infringement of any third party IPR or other rights which may result
* from the use or the inability to use the software.
*
********************************************************************************
*
* DESCRIPTION:
*   This package contains everything needed to build and run an Embedded Wizard
*   generated UI application on a STM32 target.
*   This Build Environment for Embedded Wizard generated UI applications was
*   tested by using the following components:
*   - Embedded Wizard Studio V8.20
*   - Embedded Wizard Platform Package for STM32 V8.20
*   - STM32F746 Discovery Build Environment V8.20.01
*   - STM32F746 Discovery board
*   - ST-LINK Utility V4.1.0
*   - STM32CubeF7 Firmware Package V1.8.0
*   - GCC ARM Embedded Toolchain 6-2017-q1-update
*
*******************************************************************************/

Getting started with STM32F746 Discovery board:
-----------------------------------------------
  In order to get your first Embedded Wizard generated UI application up and
  running on your STM32 target, we have prepared a detailed article, which
  covers all necessary steps.
  We highly recommend to study the following document:

  http://doc.embedded-wizard.de/getting-started-stm32f746-discovery


Getting started with Embedded Wizard Studio:
--------------------------------------------
  In order to get familiar with Embedded Wizard Studio and the UI development
  work-flow, we highly recommend to study our online documentation:

  http://doc.embedded-wizard.de

  Furthermore, we have collected many 'Questions and Answers' covering
  typical Embedded Wizard programming aspects. Please visit our community:

  http://ask.embedded-wizard.de

  Please use this platform to drop your questions, answers and ideas.


Build Environment specific changes:
-----------------------------------
* Version V8.20.01
  - IAR libraries with highest optimization.
  - GCC libraries compiled with GCC ARM Embedded Toolchain 6-2017-q1-update.
  - Comments corrected.

* Version V8.20
  - The entire Build Environment is now based on a Template approach.
    Within the subdirectory /Template you will find one common project that
    is prepared for all examples and that can be used as a starting point
    for your development.
  - The Build Environment provides now template projects for Keil MDK-ARM and
    IAR embedded workbench. You will find them in the subdirectories of the
    /Template folder. Please have a look into the related ReadMe.txt files.
  - Providing libraries for GCC, MDK-ARM and IAR.
  - Important project settings are now imported automatically to GCC make,
    IAR and Keil projects: The profile settings 'ColorFormat' and
    'ScreenOrientation' are used to control the build process without
    the need for any additional manually settings.
  - The set of provided examples has been enhanced. Please have a look into
    the subdirectory /Examples.
  - All target specific source codes (ew_bsp_xxx.c/h) that build the bridge
    between the Embedded Wizard generated code and the underlying ST drivers
    are now located within the folder /TargetSpecific.
  - Stacksize of template project is increased to 8 kByte.
  - Bugfix of leap year calculation within the function EwBspGetTime().
  - The DMA2D.c module is replaced by ew_bsp_graphics.c.
  - Support of synchronized single buffer update by using partial display update.
    This approach makes it possible to draw directly into the visible framebuffer
    without flicker or tearing effects (as long as drawing operation is fast
    enough).
  - Support of off-screen buffer is removed. Use new single buffer mode or
    double-buffering instead.
  - Memory profiling feature is now always active - just add the function call
    EwPrintProfilierStatistic( 0 ) within your main loop in case you want to get
    an overview of the current/maximum memory consumption.
  - The generated macros EMWI_SURFACE_ROTATION and EMWI_COLOR_FORMAT are now
    used for library selection.


3rdParty-Components
-------------------
The following 3rdParty-Components are used:

1. Two Level Segregated Fit memory allocator, version 3.0.
   Written by Matthew Conte, and placed in the Public Domain.
   http://tlsf.baisoku.org

2. Universal string handler

/*------------------------------------------------------------------------/
/  Universal string handler for user console interface
/-------------------------------------------------------------------------/
/
/  Copyright (C) 2011, ChaN, all right reserved.
/
/ * This software is a free software and there is NO WARRANTY.
/ * No restriction on use. You can use, modify and redistribute it for
/   personal, non-profit or commercial products UNDER YOUR RESPONSIBILITY.
/ * Redistributions of source code must retain the above copyright notice.
/
/-------------------------------------------------------------------------*/

3. STM Source code

* Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

4. Gnu Make - GNU General Public License v2 or later

5. Gnu GCC - GNU General Public License v2 or later
