@echo off

REM     Please set the ABSOLUTE installation paths of the following tools / software packages:
REM     - ARM GCC toolchain (path to gcc-arm-none-eabi)
REM     - STM32Cube_FW_F7
REM     - ST-LINK_Utility
REM     - External flash loader (ST-LINK_Utility, QSPI flash)

set Toolchain_Path=C:\STM32\gcc-arm-none-eabi
set STM32Cube_Path=C:\Progra~2\STMicroelectronics\STM32Cube\STM32Cube_FW_F7_V1.8.0
set ST-LINK_Utility_Path=C:\Program Files (x86)\STMicroelectronics\STM32 ST-LINK Utility\ST-LINK Utility
set STLINK_EXT_FLASH_LOADER=%ST-LINK_Utility_Path%/ExternalLoader/N25Q128A_STM32F746G-DISCO.stldr

REM ***********************************************************************************************
REM *** no further modifications needed below

set Title=Embedded Wizard Build Environment for STM32F746 Discovery V8.20.01

color 0A
title %Title%
setlocal enableextensions

echo *******************************************************************************
echo * Welcome to %Title%
echo *******************************************************************************
echo.

IF DEFINED BuildInitialized GOTO run

GOTO setpaths

:setpaths

:: set the path to make and its tools
set PATH=%~dp0;%PATH%
set PATH=%~dp0\Make;%PATH%

:: compiler path
set PATH=%Toolchain_Path%\bin;%PATH%

:: st-link utility path
set PATH=%ST-LINK_Utility_Path%;%PATH%

set BuildInitialized=1

:run

REM do some checks

if not exist "%Toolchain_Path%" (
  echo.
  echo Toolchain check failed:
  echo The ARM GCC toolchain was not found on the expected path:
  echo '%Toolchain_Path%'.
  echo Please install the ARM GCC toolchain into this folder
  echo or adapt the toolchain path in 'Build\devenv.cmd'.
  echo.
  pause
  exit
)

if not exist "%STM32Cube_Path%" (
  echo.
  echo STM32Cube check failed:
  echo The STM32Cube_FW_F7 package was not found on the expected path:
  echo '%STM32Cube_Path%'.
  echo Please install the STM32Cube_FW_F7 package into this folder
  echo or adapt the STM32Cube path in 'Build\devenv.cmd'.
  echo.
  pause
  exit
)

if not exist "%ST-LINK_Utility_Path%" (
  echo.
  echo ST-LINK_Utility check failed:
  echo ST-LINK_Utility not found on path:
  echo "%ST-LINK_Utility_Path%".
  echo Please install the ST-LINK_Utility or adapt the path in 'Build\devenv.cmd'.
  echo.
  pause
  exit
)

if not exist "%STLINK_EXT_FLASH_LOADER%" (
  echo.
  echo STLINK_EXT_FLASH_LOADER check failed:
  echo External flash loader for ST-LINK_Utility not found on path:
  echo "%STLINK_EXT_FLASH_LOADER%".
  echo Please check your ST-STLINK Utility installation or
  echo adapt STLINK_EXT_FLASH_LOADER in 'Build\devenv.cmd'.
  echo.
  pause
  exit
)

echo STM32Cube path:      %STM32Cube_Path%
echo ToolChain path:      %Toolchain_Path%
echo ST-LINK Utitlity:    %ST-LINK_Utility_Path%
echo ext. FLASH loader:   %STLINK_EXT_FLASH_LOADER%
echo All checks are passed!
echo.
echo Entering Embedded Wizard Template project
echo.
echo *******************************************************************************
echo *
echo * Getting started with Embedded Wizard and STM32F746 Discovery board:
echo *
echo * http://doc.embedded-wizard.de/getting-started-stm32f746-discovery
echo *
echo *******************************************************************************
echo.

cd ..\Template\Project\GCC

:: set the compiler environment variables
cmd /k gccvar.bat
